# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick automation of todomvm react UI
* Version 1.0
* [UI at test](https://todomvn/react/edit this please)

### How do I get set up? ###

* Summary of set up
- Dependencies:
node
npm

- Install cypress.
cd /your/project/path
npm init
npm install cypress --save-dev

- Opening cypress
./node_modules/.bin/cypress open

- Cypress locators.
It seems I was 'old skool' using #ids and .classes to locate elements in the page.
Cypress is no recommending to use specific locators. 
This is great, testability would be increased and UI generated labels won't be changed. I'll use, as recommended:


[data-test=new-todo]


* How to run tests



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact