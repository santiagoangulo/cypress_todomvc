/// <reference types="cypress" />

// Welcome to Cypress! > Yes @Alex, I realised 90% of my task was automated

describe('example to-do app', () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test

    // @santi thoughts:
    // Initially I did cy.visit('URL') but then I created a basedUrl entry in
    // ~/cypress/cypress.json 

    cy.visit('/')
  })

  it('displays no todo items by default', () => {
    // We use the `cy.get()` command to get all elements that match the selector.
    // Then, we use `should` to assert that there are no items in the list
    // @santi thoughts: just kept most of the cypress default code as made sense
    cy.get('.todo-list li').should('have.length', 0)
  })

  it('can add new todo items', () => {
    // We'll store our item text in a variable so we can reuse it
    const firstItem = 'My first Task'
    // I add my first task
    cy.get('.new-todo').type(`${firstItem}{enter}`)
    // Then check one task with the expected name has been created
    cy.get('.todo-list li')
      .should('have.length', 1)
      .last()
      .should('have.text', firstItem)
  })

  it('can check off an item as completed', () => {
    // Adding an element to the test
    const doneItem = 'Buy some food'
    cy.get('.new-todo').type(`${doneItem}{enter}`)

    // Finding element, navigating to containing object and check done checkbox  
    // @santi > I thought it would be good to check it actually got checked, not sure 
    // is needed, but better sure.
     cy.contains(doneItem)
      .parent()
      .find('input[type=checkbox]')
      .check()
      .should('be.checked')

    // Now that we've checked the button, we can go ahead and make sure
    cy.contains(doneItem)
      .parents('li')
      .should('have.class', 'completed')
  })

  context('Two active tasks and one completed task', () => {
    const firstActiveTask = 'Cook dinner'
    const secondActiveTask = 'Wash dishes'
    const completedTask = 'Water the plants'
    const listOfTasks = [firstActiveTask,secondActiveTask,completedTask]
    const numActiveTasks = 2
    
    // small function that takes an array of tasks and creates them.
    // I call it later on in the before each section
    const createTask = (listOfTasks) => {
      listOfTasks.forEach((task) => {
        cy.get('.new-todo').type(`${task}{enter}`)
      })
    } 

    beforeEach(() => {
      // For this context I can create a before section. 
      // Here I am preparing the data to test the scenarios contained
      // within this section.
      createTask(listOfTasks)

      // Make sure all listOfTasks.length tasks are added. And last one appears at the bottom of the list
      cy.get('.todo-list li')
        .should('have.length', listOfTasks.length)
        .last()
        .should('have.text', listOfTasks[(listOfTasks.length)-1])
      // I will complete my completedTask in the before section.  
      cy.contains(completedTask)
        .parent()
        .find('input[type=checkbox]')
        .check()
        .should('be.checked')
        .parents('li')
        .should('have.class', 'completed')

    })

    it('can filter for uncompleted tasks', () => {
      // We'll click on the "active" button in order to
      // display only incomplete items
      cy.contains('Active').click()

      // After filtering, we can assert that there are two
      // incomplete items in the list.
      cy.get('.todo-list li')
        .should('have.length', listOfTasks.length - 1)
        .first()
        .should('have.text', firstActiveTask)
      // Don't want to over tests, so only asserting for length and one task
      // on purpose

      // Assert that the task we checked off does not exist on the page.
      cy.contains(completedTask).should('not.exist')
    })

    it('can filter for completed tasks', () => {
      // We can perform similar steps as the test above to ensure
      // that only completed tasks are shown
      cy.contains('Completed').click()

      cy.get('.todo-list li')
        .should('have.length', 1)
        .first()
        .should('have.text', completedTask)

      cy.contains(firstActiveTask).should('not.exist')
      cy.contains(secondActiveTask).should('not.exist')
    })

    it('active task counter only shows number of active tasks', () => {
      // Added custom command 'selectFilterNCheckTasks' in /cypress/support/commands.js
      // Here I check num of active tasks is the same regardless the list filtering
      cy.selectFilterNCheckTasks('All',numActiveTasks)
      cy.selectFilterNCheckTasks('Active',numActiveTasks)
      cy.selectFilterNCheckTasks('Completed',numActiveTasks)
    })

    it('can delete all completed tasks', () => {
      // I click the "Clear completed" button
      cy.contains('Clear completed').click()      

      // Make sure that there are only activeTasks
      // in the list and our completedTask does not exist
      cy.get('.todo-list li')
        .should('have.length', numActiveTasks)
        .should('not.have.attr','class','completed')
        .should('not.have.text', completedTask)

      // Finally, make sure that the clear button no longer exists.
      cy.contains('Clear completed').should('not.exist')
    })
  })
})
